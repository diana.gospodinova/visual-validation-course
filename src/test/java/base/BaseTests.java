package base;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.SortableDataTablesPage;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class BaseTests {
    protected static WebDriver driver;
    protected static SortableDataTablesPage page;
    protected static EyesManager eyesManager;
    private static Properties props;

    @BeforeClass
    public static void setUp() {
        props = System.getProperties();
        try {
            props.load(new FileInputStream(new File("resources/test.properties")));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        driver = new ChromeDriver();
        eyesManager = new EyesManager(driver, "The Internet");
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
        eyesManager.abort();
    }
}
